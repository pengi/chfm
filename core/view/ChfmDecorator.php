<?php

class ChfmDecorator {
	public static function decorate($o) {
		global $config;
		$class = get_class($o);
		$styles = $config['viewfiles'][$class];
		
		#TODO: more styles:
		include( $styles['default'] );
	}
}