<?php

/********************
 * Load arguments
 */

$options = getopt("D::P:f:");

$pstring = array();
if( isset($options['D'] ) ) {
	if( is_array($options['D']) ) {
		$pstring = $options['D'];
	} else {
		$pstring[] = $options['D'];
	}
}

$properties = array();
foreach( $pstring as $ps ) {
	list($key, $val) = explode('=', $ps, 2);
	$properties[$key] = $val;
}
/*
$propcontent = file_get_contents( $options['P'] );
$propcontent_fixed = "";
foreach( explode("\n",$propcontent) as $propline ) {
	$fixedline = trim($propline);
	if(substr($fixedline,0,1) != '#') {
		print( $fixedline . "\n" );
		$propcontent_fixed .= $fixedline . "\n";
	} else {
		$propcontent_fixed .= "\n";
	}
}
$properties = parse_ini_string( $propcontent_fixed );
*/
print_r ($properties);



/*********************
 * Generate runtime configuration
 */

$config = array(
	'classpath' => array()
);
$requires = array();
$includePath = array();
$extraCode = "";

foreach( glob(dirname(__FILE__).'/modules/*Builder.php') as $builder ) {
	require( $builder );
}

/*********************
 * Build index file
 */

$header =<<<EOT
<?php
/*
 * This is an autogenerated index file.
 *
 * DON'T EDIT THIS FILE DIRECTLY. YOUR CHANGES WILL BE LOST!
 */

define('CHFM_LOADED',1);

EOT;

$footer =<<<EOT


function chfmAutoload(\$classname) {
	global \$config;
	if( array_key_exists( \$classname, \$config['classpath'] ) ) {
		include( \$config['classpath'][\$classname] );
	}
}
spl_autoload_register( 'chfmAutoload' );


\$request = new ChfmHttpRequest();
\$response = new ChfmHttpResponse();
\$chfm = new Chfm( \$config );
\$chfm->handleRequest( \$request, \$response );

\$decor = new ChfmDecorator();
\$decor->decorate( \$response );

EOT;


$f = fopen($options['f'],"w");
fwrite($f, $header);
fwrite($f, "\$config = " . var_export($config, true) . ";\n" );

if( count($includePath) > 0 ) {
	fwrite($f, "set_include_path(\n");
	foreach( $includePath as $pth ) {
		fwrite($f, "    '".$pth."' . PATH_SEPARATOR .\n");
	}
	fwrite($f, "    get_include_path());\n\n");
}

foreach( $requires as $rf ) {
	fprintf( $f, "require( '%s' );\n", $rf );
}

fwrite($f, $extraCode );

fwrite($f, $footer);
fclose($f);

