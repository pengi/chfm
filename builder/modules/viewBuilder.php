<?php

$viewdir = $properties['chfm.view.dir'];

$viewfiles = array();

foreach( scandir($viewdir.'/style/') as $style ) {
	if( substr($style,0,1) != '.' ) {
		foreach( scandir($viewdir.'/style/'.$style.'/object/') as $objfile ) {
			if( substr($objfile,0,1) != '.' ) {
				$path = $viewdir.'/style/'.$style.'/object/'.$objfile;
				$obj = basename( $path, '.php' );
				
				if( !array_key_exists($obj,$viewfiles) ) $viewfiles[$obj] = array();
				$viewfiles[$obj][$style] = $path;
				
			}
		}
	}
}


$config['viewfiles'] = $viewfiles;