<?php

$coredir = $properties['chfm.core.dir'];

$class_iter = new RecursiveIteratorIterator(
		new RecursiveDirectoryIterator(
			$coredir,
			FilesystemIterator::UNIX_PATHS
		)
	);

foreach( $class_iter as $f ) {
	print_r($f);
	if( $f->isFile() ) {
		$classpath = $f->getPathname();
		$classname = $f->getBasename('.php');
		$config['classpath'][$classname] = $classpath;
	}
}